Programozás
===========

[[_TOC_]]

Bevezetés
---------

### A számítógép felépítése

Egy számítógép sokféle alkatrészt tartalmaz, de ami programozás szempontjából fontos lehet, azok a következők:

![alaplap](motherboard.png)

 - processzor (CPU, Central Processing Unit, azaz központi feldolgozó egység). Ez az, ami az utasításokat értelmezi.
 - memória (RAM, Random Access Memory, azaz véletlen elérésű memória). Itt tárolódnak a kódok és az adatok. A véletlen elérés azt jelenti, hogy bármelyik része tetszőlegesen elérhető.
 - perifériák (IO, Input / Output, azaz ki- és bemeneti egységek). Ezek azok, amik a processzor parancsait végrehajtják.
 - csatlakozók. A legtöbb perifériának van csatlakozója (ezek azok, amiket kívülről is láthatunk, és amikhez csatlakoztathatunk eszközöket), de nem mindnek. Például a valós idejű órának és a wifinek nincs, míg a hangkártyának és a hálózati kártyának van.

A modern számítógépek esetén igyekeznek mindent, amit csak lehet, egy csipbe zsúfolni (SoC, System on a Chip, azaz komplett rendszer egy csipben).

![SoC](soc.png)

Ettől függetlenül, bár nem látszik, a belső felépítés azonos, és programozás szempontjából sincs különbség.

### Memória

A számítógép emlékezete. A processzor csak azt látja, ami itt van, mást nem. Ha egy adat nincs a memóriában, akkor azt előbb
a memóriába kell mozgatni valamelyik perifériáról, hogy használni tudjuk.

**A memória bájtok sorozata**. Egy bájt 8 bitet tartalmaz, ami azt jelenti, hogy 8 darab, van-áram / nincs-áram kétállapotú kapcsoló együtt.
Mivel minden bit a többitől függetlenül állítható, ezzel így 2\*2\*2\*2\*2\*2\*2\*2 = 2^8, azaz összesen 256 variációt kapunk. Mivel a nullát is
tárolni akarjuk, így egy bájt lehetséges értékei 0-tól 255-ig terjednek. Vagy, ha szükségünk van negatív számokra is, akkor
"elcsúsztatjuk", és -128-tól +127-ig jelenthetik a számokat (ami továbbra is 256 variáció összesen).

| x = nincs-áram, Z = van-áram | Számérték | Előjeles |
|-----------------------------:|----------:|---------:|
|                  `xxxx xxxx` |         0 |        0 |
|                  `xxxx xxxZ` |         1 |        1 |
|                  `xxxx xxZx` |         2 |        2 |
|                  `xxxx xxZZ` |         3 |        3 |
|                  `xxxx xZxx` |         4 |        4 |
|              ...             |    ...    |    ...   |
|                  `xZZZ ZZxZ` |       125 |      125 |
|                  `xZZZ ZZZx` |       126 |      126 |
|                  `xZZZ ZZZZ` |       127 |      127 |
|                  `Zxxx xxxx` |       128 |     -128 |
|                  `Zxxx xxxZ` |       129 |     -127 |
|                  `Zxxx xxZx` |       130 |     -126 |
|              ...             |    ...    |    ...   |
|                  `ZZZZ ZZxx` |       252 |       -4 |
|                  `ZZZZ ZZxZ` |       253 |       -3 |
|                  `ZZZZ ZZZx` |       254 |       -2 |
|                  `ZZZZ ZZZZ` |       255 |       -1 |

Itt most csaltunk egy kicsit az `x` és `Z` jelöléssel. A nincs-áramot tipikusan 0-val jelölik, míg a van-áramot 1-el, mert ekkor
ez egy kettes számrendszerbeli számként is értelmezhető, aminek az értéke pont a második oszloppal egyenlő.

Hogy a negatív számokat miért így feleltetjük meg, az meg azért van, mert így könnyebb számolni velük. Például 254 + 5, az 259
lenne, de egy bájton a legnagyobb érték a 255 lehet, ezért **túlcsordul**. Ez kettes számrendszerben sokkal szemléletesebb:
11111110 (254) + 00000101 (5) = 100000011 (259). Csakhogy itt az eredményhez már 9 bit kellene, de nekünk csak 8 bitünk van,
ezért a legfelső 9. bit elveszik (ezt hívják túlcsordulásnak, angolul overflow), így az eredmény egy csonka 00000011 (3) lesz.
Namost ha -2 felel meg a 254-nek, akkor ez azt is jelenti, hogy emiatt pont jó eredményt kapunk! Mivel -2 + 5 = 3! Továbbá az is
igaz, figyeljük csak meg, hogy a legelső bit a nullánál nagyobb számok esetében mindig nincs-áram, azaz `x`, míg a nullánál kissebb
számoknál mindig van-áram, `Z`. Ezért ezt a bitet hívják előjel (angolul sign) bitnek is.

A 8 bitet két 4-es csoportra osztottam a táblázatban. Méghozzá azért, mert a programozók a tizenhatos számrendszert szeretik
(más néven hexa). Ennek pedig az az oka, hogy míg a kettes számrendszerbeli számot (más néven bináris) macerás tízes számrendszerbe
átszámolni, addig tizenhatosba pofon egyszerű, mert minden 4-es bitcsoport pontosan egy számjegyet ad ki (mivel 2\*2\*2\*2 = 16).
Hexában a számjegyek a `0123456789ABCDEF`. Tehát megvan a szokásos 10 darab arab számunk, meg még 6 betű az ábécé elejéről.
A `9` értéke 9, az `A` értéke pedig 10, a `B` az 11. Az `F` értéke nem túl meglepő módon a 15. Namost vegyünk egy bájtot 8 bittel.
Mivel ez két darab 4-es csoport, ezért minden bájt értéke pontosan két helyiértékű tizenhatos számrendszerbeli számmal írható le.
Például: 10011011. Vegyük ketté 4 bites csoportokba: 1001 1011. Az első csoport az 8+1, tehát 9, míg a második 8+2+1, tehát 11. És
meg is van, ennek a kettes számrendszerbeli számnak a tizenhatos megfelelője a `9B`!

Hogy egyértelművé tegyük, melyik számrendszerről is van szó, ezért a tizenhatos (másnéven hexa) számok után `h` betűt szokás
írni, a kettes (azaz bináris) után pedig `b`-t (a tízest külön nem jelöljük). Egy másik elterjedt szokás szerint nem utánna,
hanem elé írunk valamit, hogy megkülönböztessük. Ekkor az első számjegy a 0, aminek nincs ugyan értéke, de mindenképp egyértelművé
teszi, hogy itt most egy szám következik. A második betűje jelöli a számrendszert, `x` a hexa, `b` a bináris. Tehát például az
előbbi példa `10011011b` = `0b10011011` = `9Bh` = `0x9B` = `155`.

<blockquote>
<i>Kitérő</i>: [számrendszerek](szamrend.md).

**FONTOS**: az, hogy épp hányas számrendszerben ábrázolunk egy számot, ne tévesszen meg! Attól az még ugyanaz a szám, nincs belőle
több, csak másképp írjuk le. Ha nem megy a számrendszerek közötti átváltás, az nem baj. Most elég annyit megjegyezni, hogy a
gépek 2-est használnak, a hétköznapi emberek 10-est, a programozók meg 16-ost.
</blockquote>

Minden egyes memóriabeli **bájtnak van címe**. Ezzel lehet az adott bájton tárolt értékre hivatkozni. Olyan ez, mintha egymás
után raknánk egy csomó dobozt, és mindegyikre írnánk egy sorszámot. Minden dobozban nulla és 255 közötti számú üveggolyó lehet,
és a doboz sorszáma pedig a doboz címe. Például a 3-as számú dobozban 7 üveggolyó van, a 4-esben egy sem, míg az ezredikben pedig
15 üveggolyó.

A cím nagysága processzorfüggő, a jelenlegi gépek tipikusan 2^48 bitet használnak egy címhez (azaz 2\*2\*2\*2\*2\*2\*...\*2, összesen 48-szor
összeszorozva a kettőt, ami egy baromi nagy szám, sok sok tízezer milliárd címlehetőség). Egy tipikus számítógép azonban nem használ
ennyit. Egy mai gépben jellemzően 8 giga (2^33) és 64 giga (2^36) közötti méretű RAM található, azaz ennyi bájtnyi adatot képesek
egyszerre kezelni. (1 gigabájt az 1024 megabájt; 1 megabájt az 1024 kilobájt; és 1 kilobájt az 1024 bájt. Tehát 8 gigabájt az
összesen 8\*1024\*1024\*1024 = 8589934592 bájt, azaz kicsit több, mint nyolc és félezer-millió bájt.)

Namost a 256 variáció egy bájtban nem túl sok. Hogy ezt kiküszöböljük, ezért az egymás utáni címen található bájtok együtt
kezelhetők. Például 2 egymás utáni bájt már összesen 16 bitet jelent, ami 2^16 = 65536 variáció. 4 egymás utáni címen található
bájtot együtt használva pedig 32 biten már nulla és 4 milliárd közötti értéket is tudunk tárolni. A mai gépek tipikusan 64 bit
(azaz 8 bájt) együttes kezelésére képesek, illetve speciális utasítások esetén 128, de akár 512 bitet is átfoghatnak egyszerre.

### Processzor

Az általánosan elterjedt számítógépek úgynevezett Neumann architektúrájúak, mivel Naumann János találta ki, hogy így kell
működniük. Ez azt jelenti, hogy ezek a gépek nem tesznek különbséget adat és programkód között, mindkettőt pontosan ugyanúgy, a
memóriában, számok formájában tárolják. (Ez persze csak akkor igaz, ha épp fut a program. Amikor nem, akkor a háttértárolón
(ami egy periféria) várakozik, futtatható programfájl formájában.)

A processzornak része egy úgynevezett utasításmutató (Instruction Pointer, de szokták Program Counter-nek, azaz programszámlálónak
is hívni), ami egy memóriacímet tartalmaz. Ez jelöli, hogy épp melyik utasítást kell értelmezni. A processzor egy végtelenül buta
gépezet, a következő lépéseket ismételgeti a végtelenségig (vagy amíg ki nem tépjük az áramkábelét):

1. az IP által mutatott memóriacímről beolvassa a számot
2. értelmezi a számot és végrehajta az annak megfelelő utasítást
3. az IP-t a következő utasításra állítja
4. majd mindent ismétel az 1.-es ponttól

#### Gépikód

Ahhoz, hogy a számítógép tudja, mit kell csinálni, minden utasításhoz hozzárendelnek egy-egy számot. Például a 3-as szám
jelentheti azt, hogy add össze. Vagy a 9-es meg hogy vond ki egymásból. Azt, amikor a programot ilyen számsorozat formájában
ábrázoljuk, **gépikódnak** nevezzük.

Bármennyire is hihetetlenül hangzik, a legelső számítógépeket így, közvetlenül számok beírásával programozták. És egyébként ez
semmit sem változott azóta, a processzor továbbra is csak ilyen számsorokat képes értelmezni, azonban a számsorozatok
előállítására kitaláltak mindenféle trükkös megoldást, hogy könnyebbé tegyék.

#### Assembly

A legkézenfekvőbb, hogy a számokat szöveggel helyettesítjük. Ezeket a rövid szövegeket sokkal könnyebb megjegyezni,
mint a számokat, és ezért ember számára sokkal olvashatóbbá teszik a programot.

Egyrészről a memóriacímek helyett **cimké**ket használunk (például nem azt mondjuk, hogy a 3-as számú ház, hanem azt, hogy
a piros ferde tetős. Itt a "piros ferde tetős" a cimke, ami a 3-as címnek felel meg).

Másrészről az egyes címeken tárolt, gépikódot jelentő számoknak is nevet adunk, például a fenti példában a 9-es helyett
azt mondjuk, "kivon" vagy angolul "sub" (ezeket a rövid szöveges parancsokat egyébként **mnemonik**oknak hívják).

<blockquote>
<i>Kitérő</i>: életből vett példa:<pre>
$ objdump -d ./a
...
0000000000001070 <_start>:
    1070:	31 ed                	xor    %ebp,%ebp
    1072:	49 89 d1             	mov    %rdx,%r9
    1075:	5e                   	pop    %rsi
    1076:	48 89 e2             	mov    %rsp,%rdx
...
</pre>
Itt látható, hogy a 1070h-s memóriacím cimkéje a <code>_start</code>. Itt indul a program, és ezt könnyebb megjegyezni, mint magát a számot.
Alatta pár utasítás látható, minden sorban egy. Az első oszlop a memóriacím, aztán az utasítás gépikódban (amire a korábban tárgyalt
16-os számrendszerbeli jelölést használják), végül pedig az utasítás Assembly megfelelője. Például a <code>xor</code> egy logikai művelet,
kizárásos vagy kapcsolatot jelent (vagy ez, vagy az, de nem egyszerre a kettő). A <code>mov</code> adatok mozgatására ad utasítást a
processzornak, ésatöbbi.

Jól látható, hogy egy utasítás több, egymás utáni bájtot is elfoglalhat. Ezért az IP értéke ennél a programnál lehet 1070, 1072,
1075, de ha például véletlenül 1073 lenne, akkor a processzor értelmetlen zagyvaságot olvasna ki az utasítás helyett.
</blockquote>

**FONTOS**: az Assembly nem egy programozási nyelv, hanem a gépikód szöveggel megfeleltetése. A logikája nem az emberi logikát
követi, hanem egy-az-egyben a számítógépét. Emiatt az Assembly közvetlenül gépikódra fordítható (asszemblálás), illetve a gépikód
közvetlenül vissza, Assembly-vé (diszasszemblálás).

Programozási nyelvek
--------------------

Mivel az emberek gondolkodása nem a gépekét követi, hanem sokkal inkább olyan gondolatokból áll, mint például "ha ez van, akkor
csináld azt", ezért találták ki a programozási nyelveket. Ezeken az ember számára érthetőbb mondatokkal, szövegesen lehet leírni
a programokat.

Amikor a program ilyen szöveges formában, valamilyen programozási nyelven van, azt **forráskód**nak hívjuk.

Mint korábban megtárgyaltuk, a számítógép nem képes a forráskódot értelmezni, hiszen csakis a gépikódot érti. Ezért valahogyan
át kell alakítani a forráskódot gépikódra. Erre kétféle lehetőség van, attól függően, hogy az adott programozási nyelv milyen:

1. fordított: ekkor egy speciális fordító program beolvassa a forráskódot és lementi programfájlként, ami aztán betölthető a memóriába.
2. interpretált: ekkor egy program folyamatosan olvassa be a forráskódot, értelmezi, és az utasításnak megfelelő alprogramot futtatja le.

Fontos e kettő között a kölönbség: egy fordított program nem rugalmas (ha változtatunk a forráskódon, akkor újra le kell fordítani
és újra be kell tölteni), de nagyon hatékony és gyors programot eredményez. Egy interpretált ugyanakkor rugalmas (akár menet közben
is átírhatjuk a forráskódot), de lassú, sosem lehet annyira hatékony, mint egy fordított (mert csak előre definiált alprogramokból
válogathat, nincs lehetőség a processzor összes képességének a kihasználására attól függően, hogy mit kell épp csinálni).

<blockquote>
<i>Kitérő</i>: Most biztos felkaptad a fejed, hogy a program fordításához egy fordító program kell. De hát az is egy program, és az hogy lett?
Ez amolyan melyik volt előbb, a csirke, vagy a tojás probléma, nem? A válasz erre az, hogy a legeslegelső fordítóprogramot Assemblyben
is megírták, ami épp annyit tudott csak, hogy lefordította a fordítóprogramot. Ezt követően a fordítóprogram forráskódját már a korábban
lefordított fordítóprogrammal fordították. Ah, mennyi fordítás egy mondatban :-) Ezt a folyamatot egyébként úgy hívják, hogy
"compiler bootstrap", fordítóprogram beröffentés.
</blockquote>

Mivel a programozási nyelven összetett gondolatokat írunk le, ezért az lefordítható a gép számára érthető gépikódra, ugyanakkor
az Assemblyvel ellentétben a gépikód sosem fordítható vissza teljesen programozási nyelvre (vannak okos programok, amik egész jól
visszaadják az eredeti forráskódot, de az sosem lesz tökéletes). Emiatt **roppant fontos, hogy a forráskódot mindig gondosan
megőrizzük**.

Amikor azt mondjuk, hogy a programozási nyelvek az emberi gondolkodást követik, akkor az csak azt jelenti, hogy nem a gépekét.
Ez még azért mindig nagyon messze áll attól, ahogy a hétköznapi életben gondolkodunk. Itt sokkal inkább folyamatok és matematikai
módszerek leírásáról van szó. Ez minden programozási nyelvben közös, mind a következő pár elemekből épül fel:

1. változók: hogyan tárolják és hogyan hivatkozhatunk az adatainkra
2. szekvencia: utasítások egymásutánisága
3. elágazás: feltételes programvégrehajtás
4. ciklus: utasításcsoport ismétlése egy adott feltétel teljesülése esetén
5. alprogramok: hogyan gyűjthetjünk egy rakat utasítást egycsokorba, hogy azokat többször is végrehajthassuk

Ezen elemek megléte minden programozási nyelvben közös, az azonban, hogy hogyan kell leírni ezeket, már eltér.

<blockquote>
<i>Kitérő</i>: Például BASIC nyelven az utasításokat nem lehet csak úgy egymásután írni, sorszámmal kell ellátni őket. Pythonban minden
utasítást külön sorba kell írni és gondosan szóközökkel be kell húzni, míg C nyelven az utasításokat pontosvessző zárja, és nem
számít, hogy egy sorban, vagy külön sorban vannak-e. Ugyancsak C-ben minden deklarált változó neve egy-az-egyben használható, míg
BASIC-ben a típustól függően bizonyos változók neveit <code>$</code>-al zárni. PHP-ben pedig típustól függetlenül, minden változó nevét
egyformán <code>$</code> jellel kell kezdeni.
</blockquote>

Ezeket a részleteket, hogy egy bizonyos dolgot hogyan kell egy adott programozási nyelven leírni, hívjuk **szintaxis**nak.

Megjegyzés: a legtöbb programozási nyelvben lehetőség van arra, hogy megjegyzéseket tegyünk a forráskódba. Ezeket nem értelmezi
a fordító / interpreter, hanem csak átugorja. Ezekbe tipikusan a programozóknak szóló emlékeztetők és magyarázatok kerülnek.
A C nyelv esetén ezeket a megjegyzéseket `/*` és `*/` közé kell tenni.

### Változók

Ez a változók fejezet egy kicsit hosszabb és összetettebb lett. Sajnos nem lehetett lerövidíteni, mert mindent fontos megérteni,
ami itt van. Vígasztalástul a maradék fejezetek mindegyike sokkal, de sokkal rövidebb ennél!

Az minden nyelvben közös, hogy a változók cimkék, azaz névvel tudunk hivatkozni rájuk, nem kell tudnunk, hogy pontosan melyik
memóriacímen helyezkednek el. Az is közös, hogy minden nyelv kezeli, mekkora a változó mérete, azaz hány egymás utáni bájtot
foglal el azon az adott memóriacímen.

A változónak lehet azonban típusa is, ami nem jellemző minden nyelvre. Azokat a nyelveket, ahol kötelező a típust megadni
(ezt deklarásának hívjuk), erősen típusos nyelveknek nevezzük. Főként a fordított nyelvek ilyenek. Azokat a nyelveket, ahol a
deklarálás nem kötelező, azokat pedig gyengén típusos nyelvnek (jellemzően az interpretált nyelvek). Miért hasznos a változó
típusa? Azért, mert nemcsak azt mondja meg, hogy a változó mérete mekkora, hanem azt is, hogy milyen műveletek végezhetők vele,
így sokkal kissebb a hibázás lehetősége.

Például C nyelven deklarálhatunk egy változót a következő utasítással
```c
int32_t almaszelet = 0;
```
Itt az `int32_t` jelenti a típust (integer, azaz számérték), ami megadja, hogy 32 bites (azaz 4 egymás utáni bájtot foglal a memóriából),
előjeles (azaz a lehetséges értékei -2milliárdtól a +2milliárdig terjednek). De ennél többet is elárul, ugyanis mivel a C erősen
típusos nyelv, ezért a fordító program számára egyértelmű, hogy milyen műveletek végezhetők ezzel a változóval (azaz milyen
gépikódú utasításokra van szükség, ha például összeszorozzuk egy másik számmal. Ha a fordítóprogramnak nincs ismerete arról,
hogy milyen gépikódú utasítások kellenek, akkor az az adott művelet nem elvégezhető, és hibát dob). A változó cimkéje
`almaszelet`, ami beszédes, így a programozó egyből látja, hogy ebben a változóban bizony az almaszeletek számát tárolja a program.
Végezetül az értékadás azt jelenti, hogy nincs egyetlen almaszelet sem, tehát a cimke által jelölt memóriacímre 4 bájtnyi nulla
kerül a program indulásakor.

#### Egyszerű változók

Ezek olyan változók, melyek egyetlen érték tárolására alkalmasak. Minden nyelvben megtalálhatóak, és többnyire a nyelv részeként
jönnek (azaz nem felülbírálhatók a programozó által). Szokták **skalár**oknak vagy alaptípusoknak is hívni őket.

A C nyelv alaptípusai:

| típus               | méret | min                  | max                  | leírás                             |
|---------------------|------:|---------------------:|---------------------:|------------------------------------|
| `void`              |     ? |                    ? |                    ? | ez a "nincs típusa", azaz semmi típus |
| `char`              |     1 |                 -128 |                  127 | előjeles egész szám                |
| `unsigned char`     |     1 |                    0 |                  255 | előjel nélküli, pozitív egész szám |
| `short`             |     2 |               -32768 |                32767 | előjeles egész szám                |
| `unsigned short`    |     2 |                    0 |                65535 | előjel nélküli, pozitív egész szám |
| `int`               |     4 |          -2147483648 |           2147483647 | előjeles egész szám                |
| `unsigned int`      |     4 |                    0 |           4294967295 | előjel nélküli, pozitív egész szám |
| `long`              |     8 | -9223372036854775807 |  9223372036854775807 | előjeles egész szám                |
| `unsigned long`     |     8 |                    0 | 18446744073709551615 | előjel nélküli, pozitív egész szám |
| `float`             |     4 |           +/- 2^-126 |            +/- 2^127 | egyszeres pontosságú lebegőpontos  |
| `double`            |     8 |          +/- 2^-1022 |           +/- 2^1023 | dupla pontosságú lebegőpontos      |

Az `int` és a `long` mérete azonban processzorfüggő. Lehet 2 bájt, de 8 bájt is, attól függően, mekkora mérettel számol
a leghatékonyabban az adott processzor (a C nyelv ilyen fifikásan lett kitalálva, az `int` mérete mindig az adott processzortól
függ). Hogy az ebből adódó problémákat elkerüljük, azért az `int`-nek vannak konkrét változatai, bitszámokkal, például `int8_t`
egy olyan előjeles szám, ami 8 bites, azaz egy bájtnyi, pont, mint a `char`. Az előjel nélküli változata az `uint8_t`. Ebből a
előjeles - előjelnélküli párosból van 8, 16, 32, 64 és 128 bites is, például `uint128_t` (ami már 16 egymás utáni bájtot foglal el).

A lebegőpontos számok kicsit trükkösek. Nem elcsúsztatják az ábrázolt értéket, mint az integer típusú változók, hanem
külön bitet foglalnak az előjelnek, ezért például képesek tárolni a `0`-át és a `-0`-át is. Főleg matematikai számításoknál
használatosak, egyébként nem túl hasznosak. (Egy 3D-s játékban mondjuk rengeteg matematikára van szükség.)

#### Összetett változók

Ezek olyan változók, amik több értéket is képesek tárolni. Alapvetően kétféle van belőlük:

- **tömb**: ez olyan változók gyűjteménye, ahol minden elem azonos típusú, és a tömb mérete (elemszáma) akár menet közben is változtatható.
- **struktúra**: ez pedig olyan, ahol az elemek típusa eltérhet, és a méret a fordításkor dől el, később nem változtatható.
- illetve ezek tetszőleges kombinációja, például tömböket tartalmazó struktúra, vagy struktúrákat tartalmazó tömb. De akár tömböket tartalmazó tömb is lehet.

Példa tömbre:
```c
char jatekos_neve[32];
```
Ez egy olyan tömb, ahol minden elem `char` típusú, és összesen 32 van belőlük (mivel egy `char` mérete 1 bájt, ezért a tömb teljes
mérete 32 bájt). Kiválóan alkalmas mondjuk a játékos nevének tárolására, ami szintén karakterek egymásután. Az ilyen karaktertömböket,
ha a legutolsó tárolt karakter értéke 0, **sztring**eknek nevezzük. Az utolsó lezáró 0 azért fontos, mert így különböző hosszúságú nevek is
tárolhatók, nem kell mindnek pontosan 32 betűt tartalmaznia. Emiatt viszont nem lehetnek hosszabbak, mint 31 bájt (mivel a tömbünk teljes
méretének 32-őt adtunk meg, és egy bájtnyi helyet elfoglal a lezáró 0).

<blockquote>
<i>Kitérő</i>: ez nem mindig volt így, de a számítógépek jelenleg az [UNICODE](https://hu.wikipedia.org/wiki/Unicode) táblát használják,
ami betűket és más írásjeleket feleltet meg a memóriában tárolt számoknak. Például a 65-ös szám jelenti a nagy <code>A</code> betűt, a 66-os pedig a
nagy <code>B</code>-t. (Érdekesség, hogy még a magyar rovásírás betűínek is van ilyen UNICODE száma, nemcsak a latin ábécének.)
</blockquote>

A tömb egyes elemeire indexekkel hivatkozhatunk, ami 0-tól a tömb mérete-1 -ig tart. Ebben az esetben `jatekos_neve[0]` adja az
első karaktert, míg a `jatekos_neve[31]` az utolsót. Elsőre furcsa lehet, hogy 0-tól indul a számozás, nem 1-től. Ez azért van,
mert egy adott elem memóriacímét úgy kapjuk meg, hogy a `változó címe + index * egy elem mérete`. Mivel az elemek szépen sorban
egymás után vannak a memóriában, ezért az első elem címének ugyanannak kell lennie, mint magának a tömb címének. Tehát
tegyük fel, hogy `jatekos_neve` a 1070-es címre kerül. Ekkor a legelső elem címének is ugyanennek kell lennie, tehát a
`jatekos_neve[0]` címe szintén 1070. A második elem címe a tömb címe plusz egy elem mérete, tehát `jatekos_neve[1]` a 1071-re
kerül. Így jön ki, hogy elem címe = tömb címe + index \* elemméret.

Struktúra esetén kicsit bonyolultabb a dolog, mert érdemes típusként is definiálni, hogy a későbbiekben többször felhasználhassuk.
Példa struktúrára:
```c
typedef struct {
    int x;
    int y;
    int z;
} pont3D;

pont3D koordinata;
```
Itt először azt mondtuk, hogy definiálunk egy összetett típust, a 3 dimenziós pontot, aminek három eleme van, itt most mind `int`
típusú (de lehetnének eltérő típusúak is akár). Aztán, definiáltunk egy változót, a `koordinata`-t, aminek a típusa egy ilyen
összetett 3 dimenziós pont típus (mivel minden egyes elem `int` típusú, ezért a változó mérete 3\*4, azaz összesen 12 bájt).

A struktúra elemeire a változó nevével, egy ponttal, majd a mező nevével hivatkozhatunk. A fenti példánk esetében `koordinata.x`,
`koordinata.y` vagy `koordinata.z`. Megtehettük volna, hogy külön definiálunk 3 változót, de majd látni fogjuk, hogy megéri az
egymáshoz kapcsolódó változókat így egy struktúrába összefogni.

Ezeket az összetett típusokat kombinálhatjuk is. Például egy játékban szükség lehet az elért pontok tárolására (hi-score). Ehhez
érdemes egy olyan tömböt fabrikálni, aminek az elemei struktúra típusúak:
```c
typedef struct {
    char jatekos_neve[32];
    int pontszam;
} hiscore;

hiscore dobogo[10];
```
Ez azt jelenti, hogy egy olyan `dobogo` nevű tömbünk lesz, aminek 10 eleme van, és minden elem tartalmazza a játékos nevét
és elért pontszámát. Például a harmadik helyezett pontszámára így hivatkozhatunk: `dobogo[2].pontszam` (az index 0-tól indul).

#### Mutatók

Néhány nyelvben megtalálható egy nagyon vicces változótípus, a **mutató** (angolul pointer). Ez egy olyan változó, amiben nem érték,
hanem egy memóriacím van. Ezért a méretük mindig ugyanakkora, függetlenül attól, hogy mire mutatnak. A legtöbb programozási nyelv nem
ismer ilyent, még azt is le akarják tagadni, hogy a változók igazándiból memóriacímeknek felelnek meg. A C nyelv azonban nemcsak hogy
nem rejti ezt el, hanem elég erőteljesen épít is rá és a mutatókra, ezért fontos megismerkedni velük.

<blockquote>
<i>Kitérő</i>: nem is olyan rég, pár éve még a mutató mérete 4 bájt volt, azaz egy <code>uint32_t</code>, mivel az akkori gépek
csak 32 bites címet tudtak kezelni (ugye azért 4 bájt, mert egy bájt 8 bit, és 4 * 8 az 32 bit). Mint a <i>Memória</i> fejezetben
írtuk, a mai gépek viszont legalább 48 bites címeket használnak, ami már nem férne ebbe bele. A legkissebb C változótípus, amibe
belefér egy ekkora cím, az az <code>uint64_t</code>, azaz 64 bit, ami 8 bájt.
</blockquote>

Korábban definiáltunk egy változót, így:
```c
int32_t almaszelet = 0;
```
Ennek a címét mi nem ismerjük (azt a fordító magától kezeli), csak a méretét, ami 4 bájt és a tartalmát, ami itt most nulla. Ezzel
szemben ha egy csillagot teszük a név elé,
```c
int32_t *almaszelet_mutato = 0;
```
akkor egy mutatót kapunk, aminek a mérete 8 bájt, és az értéke pedig egy memóriacím. Mellesleg mivel int32_t mutatóról van szó,
ezért egy 4 bájtnyi memóriára mutat, amiben egy szám van. De magának a mutatónak az értéke nem ez a szám, hanem egy memóriacím,
méghozzá jelen esetben a nullás cím.

Namost amikor arra vagyunk kíváncsiak, hogy egy változóban milyen érték van, akkor csak a nevét kell leírni, például
`almaszelet`. Azonban ha ezt egy mutatóval tesszük, mint például `almaszelet_mutato`, akkor az természetesen egy memóriacímet fog
visszaadni, hiszen a mutató értéke a memóriacím. Hogy azt kapjuk meg, ami azon a bizonyos memóriacímen van, nem pedig magát a címet,
ahhoz a C nyelv egy speciális "címfeloldás" operátort használ, ez egy csillag `*`, amit mindig a cím elé kell írni. Mivel a mutató
neve eleve egy címet ad vissza, ezért a mutató neve elé is írhatjuk a csillagot amikor hivatkozunk rá, például `*almaszelet_mutato`.
(Elsőre kicsit zavaró lehet, hogy a deklarációban és a hivatkozáskor is csillagot kell a neve elé tenni, de ez az azonosság igazából
csak segít, és olvashatóbbá teszi a forráskódot.)

<blockquote>
<i>Kitérő</i>: itt most a nullás címet tettük a mutatóba. Igazából amikor az operációs rendszer betölti a programot valamelyik
perifériáról a memóriába, akkor sosem használja a nullás címet, ezért a nullás címet úgy szokás értelmezni, hogy "nem mutat sehova"
vagy másképp "nincs megadva". Ezért ha a nullás címre hivatkozunk csillaggal, akkor hibát fog dobni a programunk. Ez a memóriacím
egy külön jelölést is kapott a C nyelvben, ami a <code>NULL</code>. A <code>*NULL</code> mindig, garantáltan bekrepáltatja a
programunkat.
</blockquote>

Hogy adjuk meg, hogy milyen címet tartalmazzon a mutató, ha azt még mi sem tudjuk, hiszen azt a fordítóprogram osztja ki? Erre
való a "címe" operátor, a `&`. Ezt ugyancsak a változó neve elé kell írni, mint a csillagot. Például:
```c
int32_t almaszelet, *almaszelet_mutato;         /* deklarálunk egy változót és egy mutatót */

almaszelet = 3;                                 /* a változó memóriacímére (akármi is legyen az), 4 bájton 3,0,0,0 kerül */

almaszelet_mutato = 0;                          /* a mutató most a 0-ás memóriacímre mutat */
*almaszelet_mutato = 5;                         /* mivel a 0-ás cím nem használható, hibát fog dobni a programunk. Azonban ha... */

almaszelet_mutato = &almaszelet;                /* a mutatóba érvényes cím, a változó címe kerül (akármi is legyen az), akkor a */
*almaszelet_mutato = 5;                         /* csillagos forma a mutatott változó értékét változtatja */
/* az almaszelet változó értéke most már 5, és nem 3 */
```

Ez a címfeloldás művelet az, ami a gépikódú parancsban van, és amit a processzor igazából használ, és ez az, amit igyekeznek
elrejteni a különféle programozási nyelvek. A C viszont nem szívbajos, megengedi a használatát, csak annyit köt ki, mindig `*`
illetve a `&` karakterekkel kell hivatkozni rájuk, függetlenül attól, hogy épp milyen processzorra fordítunk, és annak mi is a
gépikódja.

Ha még emlékszünk, a tömböknél megadtuk, hogy egy elem mekkora, és hogy hány eleme van, például:
```c
char jatekos_neve[32] = "Kaponyányi Monyok";
```
Itt az első elemre, a név első betűjére a `jatekos_neve[0]` formában hivatkozhattunk. Bármennyire is meglepő, de a számítógép
számára ez pont ugyanaz, mintha mutatót használtunk volna. Például írhattuk volna azt is, hogy
```c
char *jatekos_neve = "Kaponyányi Monyok";
```
Ekkor ugye ha a mutatott értékre vagyunk kíváncsiak, az első betűre, akkor arra a `*jatekos_neve` formában hivatkozhatunk.
Csakhogy, továbbra is működik a `jatekos_neve[0]` forma! Sőt mi több, mivel a második betű az első után van a memóriában, ezért a
második betű címe eggyel több, mint az elsőjé, tehát a második betűre úgy kell hivatkozni, hogy `*(jatekos_neve + 1)` (azaz a
mutató értéke plusz egy, és ezt oldjuk fel). De ehelyett is használhatjuk a `jatekos_neve[1]` formát! Ez azért van így, mert a
processzornak teljesen mindegy, hogy mutatóról vagy tömbről van-e szó, egy adott elemet mindkét esetben úgy éri el, hogy előbb
a `változó címe + index * egy elem mérete` képlettel kiszámolja a címét.

### Szekvencia

Mint korábban említettük, a C nyelven a parancsokat úgy tudjuk egymás után írni, hogy pontosvesszővel `;` zárjuk azokat.
Innen tudja a fordítóprogram, hogy egy utasítás végére ért. Általában minden utasítást szokás külön sorba írni, de a C nyelv
eléggé megengedő, ez nem kötelező.

Több utasítást egybe is foglalhatunk, egy csokorba. Ezt **blokk**nak hívjuk, és a `{` jelöli a kezdetét, valamint a `}` a végét.
Alapból teljesen lényegtelen, hogy használunk-e blokkokat vagy sem, az utasítások pont ugyanúgy fognak végrehajtódni, egymás
után. Azonban mindjárt látni fogjuk, hogy bizonyos helyzetekben fontos szerepük van.

### Elágazás

A C nyelvben kétféle elágazás lehetséges. Az első, és tipikusabb (ez minden más nyelvben is megtalálható) a **logikai elágazás**:
```c
if( kifejezés )
    utasítás1
```
Ekkor a megadott `utasítás1` csakis akkor hajtódik végre, ha a megadott `kifejezés` értéke nem nulla (azaz "igaz", ez nemcsak
az 1-et jelenti, hanem bármilyen más, nem nulla számot is). Van egy olyan lehetőség is, hogy nullánál mit csináljon a program
(azaz amikor a kifejezés "hamis"):
```c
if( kifejezés )
    utasítás1
else
    utasítás0
```
Ekkor vagy az `utasítás1`, vagy az `utasítás0` hajtódik végre, de sohasem mindkettő. Az is jellemző, hogy nemcsak egy utasítást
akarunk végrehajtani, hanem többet. Ekkor lesz nagyon hasznos a `{}` blokk. Mindezt figyelembe véve C nyelven egy tipikus elágazás:
```c
if( almaszelet > 0 ) {
    printf("még van! nyamm!");
    almaszelet = almaszelet - 1;
} else {
    printf("már mind felzabáltad, csórikám!");
}
```
Ez a mintaprogram kétféle szöveget írhat ki, attól függően, hogy az `almaszelet` változó értéke nagyobb-e nullánál vagy sem. Ha
még van, akkor egyben csökkenti is az almaszeletek számát. Ezt az értékadással éri el, az `almaszelet` változó értéke legyen
eggyel kissebb, mint a mostani változó értéke. Ez lényegében egyet kivon a változó értékéből. (A `printf()` függvény egy szöveget
ír ki a képernyőre, erről majd később.)

Ez a fajta elágazás mindössze kétféle ágat tud kezelni, attól függően, hogy a megadott kifejezés igaz-e vagy sem. Viszont a C
nyelv ismer egy másik elágazást is, amikor a kifejezés minden lehetséges értéke pontosan egy programblokkot választ ki több
lehetőség közül. Ez az **esetválasztás**:
```c
switch( kifejezés ) {
    case (érték1):
        utasítás
    break;
    case (érték2):
        utasítás
    break;
    case (érték3):
        utasítás
    break;
      ...
    default:
        utasítás
    break;
}
```
A `default` (alapértelmezett) jelenti az összes olyan értéket, amit korábban külön nem soroltunk fel valamelyik `case` (eset) ágban. Páldául:
```c
switch( almaszelet ) {
    case 0:
        printf("már mind felzabáltad, csórikám!");
    break;
    case 1:
        printf("még van egy utolsó szelet");
    break;
    case 2:
        printf("még van egy pár");
    break;
    case 3:
        printf("már csak három szelet maradt");
    break;
    default:
        printf("még van dögivel");
    break;
}
```

### Ciklus

Kétféle ciklus van a C nyelven (na jó, három). Az első az **elöltesztelős ciklus**, ami pont úgy néz ki, mint az elágazás:
```c
while( kifejezés )
    utasítás
```
Ez addig ismétli az `utasítás`-t, amíg a `kifejezés` nem nulla. Ha a `kifejezés` már eleve nulla, akkor az `utasítás` <i>egyetlen
egyszer sem</i> fog végrehajtódni. Mivel itt is tipikus, hogy nemcsak egy utasítást akarunk ismételni, ezért itt is hasznos a `{}`
blokk.

A másik fajta ciklus a **hátultesztelős ciklus**:
```c
do {
    utasítás
} while( kifejezés );
```
Ez nagyon hasonlít az előzőre, az összes különbség annyi, hogy a blokk, amit ciklusmagnak is szokás hívni, <i>legalább egyszer</i>
mindenképpen lefut.

Példa:
```c
almaszelet = 5;
while( almaszelet > 0 ) {
    printf("még van %d almaszelet.\n", almaszelet);
    almaszelet = almaszelet - 1;
}
```
(A `printf`-re még visszatérünk, a `\n` azt jelenti, újsor (angolul newline), ez új sorban kezdi a következő kiírást.) Ez a program a
következőt írja ki:
```
még van 5 almaszelet.
még van 4 almaszelet.
még van 3 almaszelet.
még van 2 almaszelet.
még van 1 almaszelet.
```

Végezetül van a **számlálós ciklus**, ami pedig így néz ki:
```c
for( indulás ; kifejezés ; léptetés ) {
    utasítás
}
```
Ez igazándiból nem egy új típus, csak kényelmi dolog (angolul ezt úgy hívják, hogy "syntactic sugar", azaz "cukormázas szintaxis"),
és pontosan ugyanaz, mintha azt írtuk volna, hogy:
```c
indulás
while( kifejezés ) {
    utasítás
    léptetés
}
```
Csak épp nem kell ennyi sorba tördelni a dolgokat, hanem egymásután kerül, ami összetartozik. Az előbbi példa `for`-al:
```c
for( almaszelet = 5; almaszelet > 0; almaszelet = almaszelet - 1 ) {
    printf("még van %d almaszelet.\n", almaszelet);
}
```

Roppant fontos, hogy a ciklus `kifejezés`-ét a ciklusmagon belül mindig megváltoztassuk (azaz a kifejezésbeli változót léptessük),
különben egy **végtelen ciklus**-t kapunk. Ez egy olyan ciklus, ami megakasztja a programunk futását, és addig ismétlődik, míg a
futást meg nem szakítjuk, vagy közismertebb kifejezéssel élve, amíg a programot le nem lőjjük.

Azonban előfordulhat, hogy direkt akarjuk kiakasztani a programot. Ekkor a legegyszerűbb forma, ha a kifejezésnek 1-et választunk
(ami egészen tutibiztosan sohasem nulla), ciklusmagnak meg nem kell semmi:
```c
while(1);
```
Ennek hatására a processzor a következőt fogja végrehajtani: az 1 az nem 0? Nem, tehát nézzük meg, hogy az 1 az még mindig nem 0?
Még mindig nem, tehát nézzük meg, hogy az 1 az még mindig nem 0? Biza még mindig nem, tehát nézzük meg, hogy az 1 az 0? És így
tovább a végtelenségig.

A ciklusmagban használható két speciális utasítás, a `continue` (folytatás), illetve a `break` (megszakítás). Az előbbi hatására
a ciklusmag futtatását újrakezdi a processzor. A másodikra pedig a ciklus utánni utasítással folytatja. Például, tegyük fel,
hogy ki akarjuk rajzolni képekkel a játékos nevét a képernyőre. A szóközt ugye nem kell, mivel az üresség, nincs hozzá kép. Azt
meg korábban megbeszéltük, hogy a sztringeket egy nulla karakter zárja le. Ezért ebből adódik a következő példa:
```c
int i;
char jatekos_neve[32];

for( i = 0; i < 32; i = i + 1 ) {
    if( jatekos_neve[i] == ' ' )
        continue;
    if( jatekos_neve[i] == 0 )
        break;
    kirajzol( jatekos_neve[i] );
}
```
Ez a ciklus a következőképp működik: van egy `i` számlálónk (ez egyébként jellemző, hogy `i` az integer számláló neve), ami
0-tól megy 31-ig (32 karakterünk van a `jatekos_neve` tömbben, 0-tól 31-ig, és az `i < 32` kifejezés azt jelenti, hogy az `i`
32 már nem lehet, tehát így pont minden tömbindexen végigszalad). Minden egyes elem esetén a ciklusmag először is megnézi, hogy
az adott elem szóköz-e. Ha igen, akkor folytatja a ciklust, a ciklusmag többi része már nem fut le (kivéve a léptetést). Ha
azonban nem szóköz, akkor megnézi, hogy nulla karakter-e. Amennyiben igen, akkor megszakítja a ciklus futását, hiszen a nulla
karakter a sztring végét jelzi, nincs már több betű, amit ki kéne rajzolni. Ha ez a feltétel sem teljesül, akkor az adott tömbelem
se nem szóköz, se nem nulla, tehát az ábécé egy betűje, amit ki kell rajzolni. (Hogy a `kirajzol` függvényt pontosan hogyan kell
megírni, hogy szép színes ábrákat jelenítsen meg minden betű helyén, abba most ne menjünk még bele.)

### Alprogramok

És elérkeztünk a programokat felépítő utolsó építőkockához, az alprogramhoz. Ennek az angol elnevezése function (ha ad vissza
értéket), illetve subroutine (ha nem ad vissza). Emiatt a dupla elnevezés miatt sok programozási nyelvben külön szintaxisa is van
ennek a kettőnek. De nem a C nyelvben, ott csak egy. Az összes különség annyi, hogy a rutinok visszatérési értéke a semmi, azaz
a `void`.

Ahhoz, hogy egy funkciót használhassunk, előbb tudatni kell a fordítóprogrammal, hogy van ilyen (meg kell adni a **prototípus**át).
Ez majdnem teljesen ugyanúgy néz ki, mint a változó deklarálás, csak épp zárójeleket kell használni a név után, és értelemszerűen
nem adhatunk neki induló értéket.
```c
int32_t almaszelet();
```
Ezt követően a programunkban használhatunk `almaszelet()` hívást, ami egy 32 bites számot fog visszaadni, pont, mint a korábbi
változónk. Vegyük észre a kölönbséget: az `almaszelet` egy változó, ami egy számot tárol a memóriában, és azt adja vissza, míg
az `almaszelet()` egy függvény, ami minden híváskor lefuttat egy alprogramot és az eredményét adja vissza. (A híváskor nem kell
tudnunk, hogy pontosan hogyan is működik az a függvény, bőven elég annyit tudni, hogy majd számot fog visszaadni.)

Azonban ahhoz, hogy lefordíthassuk a programunkat, nem elég megadni egy függvény prototípusát, itt már meg kell adni a függvény
alprogramjának kódját. Ahhoz, hogy a fordítóprogram tudja, mit kell csinálnia annak a függvénynek, szükség van egy programtörzsre
is. Ezt hívják **implementálás**nak. Hogy ezt honnan szerezzük, arra két lehetőség van:

1. megírjuk mi magunk
2. behúzunk egy függvénykönyvtárat, amiben valaki más már megírta nekünk

Például a korábban már említett `printf` implementációja, ami szöveget ír ki a képernyőre, az alapértelemezett függvénykönyvtárban
található (`libc`), ezt nem kell nekünk megírni, csak a prototípusára van szükségünk és máris használhatjuk.

Másrészről azt még senki nem írta meg, hogy az `almaszelet()` függvény mit csináljon, ezért ezt nekünk kell implementálni. A függvény
implementációja semmi más, mint pontosvessző helyett egy `{}` utasításblokk. Ebben, ha van a függvénynek visszatérési értéke,
használni kell egy speciális `return` utasítást, ami megadja, hogy mi is az az érték, amit vissza kell adni. Ennek az értéknek a
típusa természetesen meg kell egyezzen a függvény típusával.
```c
int32_t almaszelet() {
    printf("most jól megszámolom...\n");
    return 5; /* itt most int32_t számot kell használni */
}
```
Ennyi. Nézzünk egy másik példát alprogramra:
```c
void sokszokoz() {
    printf("    ");
}
```
Itt a függvény típusa `void`, azaz semmi, tehát nem ad vissza semmit, így nem is kell a `return` utasítás. Persze ha akarnánk,
akkor használhatnánk a `return;`-t, érték nélkül.

A függvényeknek adhatunk paramétereket. Ezek változódeklarációk, vesszővel elválasztva, a két zárójel között. Ennek az az értelme,
hogy ezeknek a paraméterváltozóknak a függvény hívásakor adhatunk értéket, a függvénytörzsben pedig ugyanúgy használhatjuk őket,
mint bármelyik másik változót. Például:
```c
int osszead( int a, int b ) {
    return a + b;
}

int eredmeny = osszead(1, 2);
```
Vagy
```c
void sokszokoz( int mennyi ) {
    int i;

    /* ha kevesebb, mint 1 szóközt kéne kiírni, akkor inkább egyből visszatérünk */
    if( mennyi < 1 )
        return;

    for( i = 0; i < mennyi; i = i + 1 )
        printf(" ");
}

/* tíz szóköz kiírása */
sokszokoz(10);
```

És ezennel el is értünk ennek a programozási bevezetőnek a végéhez. Már csak egyetlen egy dolog maradt, a főprogramunk. A C nyelvben
ez is csak egy ugyanolyan függvény, mint a többi, az utasításblokkja lesz a programunk maga. Mielőtt megadnánk a prototípusát, idézzük
fel, hogy a sztringek olyan karaktereket tartalmazó tömbök, amiknek az utolsó eleme a 0 karakter, valamint hogy a mutatók és a
tömbök a processzor számára lényegében ugyanazok. Tehát ha egy olyan tömböt szeretnénk, amiben sztringek vannak, akkor ennek a
karaktertömböket tartalmazó tömbnek a típusa C-ben `char **`.
```c
int main(int argc, char **argv)
{
    return 0;
}
```
Ez minden C program fő függvénye. Amikor elindítjuk a programot, akkor ez fut le. Paraméterként kap egy számot, a sztringek számát
az `argc` változóban, illetve magukat a sztringeket, az `argv` tömbben. Ha például ki szeretnénk iratni, hogy milyen paraméterekkel
indították a programunkat, akkor azt így tehetjük meg:
```c
int main(int argc, char **argv)
{
    int i;

    /* annyiszor ismételjük, ahány paramétert kaptunk */
    for( i = 0; i < argc; i = i + 1 ) {
        printf("%d. paraméter: %s\n", i, argv[i]);
        printf("%d. paraméter első betűje: %c\n", i, argv[i][0]);
    }

    return 0;
}
```

A továbbiakban belemegyünk részletesebben a C nyelv lehetőségeibe, és hogy hogyan kell bizonyos egyszerű algoritmusokat megírni
benne, például hogyan derítsük ki, hogy egy tömbben melyik a legnagyobb elem, vagy ha adott egy tömb, amiben sorba vannak
rendezve az elemek, akkor hogyan találjuk egy adott elem indexét piszkosul gyorsan.

Ezt fogja követni a C nyelv alap függvénykönyvtárának a részletes bemutatása, illetve az SDL nevű függvénykönyvtáré, amivel
képeket rajzolhatunk a képernyőre, valamint hangokat szólaltathatunk meg.

Ha mindez megvan, csak akkor kezdhetünk egy nagyon egyszerű játék megírásába. Odáig még bizony hosszú az út.
