Számrendszerek
--------------

Ezen a ponton muszáj szót ejteni a szemrendszerekől, ezt fontos megérteni.

Az iskolában és a hétköznapokban a tízes számrendszert használjuk. Ennek az oka az, hogy tíz ujjunk van a kezünkön. De nem
feltétlenül kellene ennek így lennie. Például a civilizáció hajnalán, az ókorban a mezopotámiaiak 6-os és 12-es számrendszert
használtak (a hat azért praktikus, mert a legkisebb páros szám, a 2 és a legkissebb páratlan szám, a 3 szorzata). Ezt egyébként
mind a mai napig megőriztük, ezért van az óralapon 12 óra és nem 10. A középamerikai őserdők indiánjai, a maják nem hordtak cipőt,
így számukra kézenfekvő volt, hogy nemcsak kézujjaik, de lábujjaik is vannak, ezért ők például a 20-as számrendszert használták.

De visszatérve, hogy minek is vannak a számrendszerek. Ha akarnánk, használhatnánk minden számra egy szimbólumot. Ezt megtehetnénk,
a gond csak az, hogy végtelenül sok szám van, ezért végtelenül sok szimbólumot kéne fejben tartanunk. Ehelyett az emberek
(egymástól függetlenül, különböző korokban és helyeken) kitalálták a számrendszereket. Itt csak pár szimbólumra van szükség, és
ezek kombinálásával írjuk le az egyes számokat. A kombináláshoz helyiértékeket használnuk, ahol minden helyiértéken pont számrendszer
számú szimbólum állhat, nem több. Mivel a nulla is szám, ezért egy N-es számrendszer esetén `0`-tól `N-1`-ig vannak a szimbólumok
(ami nem túl sok, tehát könnyű megjegyezni). Például a jólismert tízes számrendszerben 10 szimbólumunk van, az úgynevezett arab
számok: `0123456789`. Tizenhatos (vagy hexa) számrendszerben pedig tizenhat szimbólumunk: `0123456789ABCDEF`.

Tars ki, minden helyiérték értéke az ott lévő szimbólum értéke és a számrendszer hatványának a szorzata. A hatványokat úgy kapjuk,
hogy a számrendszer értékét önmagával szorozzuk, minden helyiértéken eggyel többször. Tehát N-es számrendszer esetén például:
```
x*1 + x*N + x*N*N + x*N*N*N...
```
A 10-es számrendszer esetén ezek a hatványok az 1, 10, 10\*10=100, 10\*10\*10=1000, ésatöbbi. Kettes számrendszer esetén a
hatványok 1, 2, 2\*2=4, 2\*2\*2=8, 2\*2\*2\*2=16 ésatöbbi. Hogy ne legyen túl egyszerű a dolgunk, ezeket a hatványokat fordított
sorrendben szokás írni, azaz a legkissebb van a legvégén, a legnagyobb pedig az elején.

Egy adott szám ezen helyiértékek és hatványok szorzatának összege. Például, a 123 értéke azért 123, mert
```
1*10*10 + 2*10 + 3*1 = 123
```
Itt a szorzatok első tagját egybeolvasva kapjuk, hogy `123`.

Ugyanez 16-os számrendszerben:
```
7*16 + B*1 = 123
```
Ugyancsak a szorzatok első tagját egyeolvasva megkapjuk, hogy hexában ugyanez a szám `7B`.

Illetve 2-esben:
```
1*2*2*2*2*2*2 + 1*2*2*2*2*2 + 1*2*2*2*2 + 1*2*2*2 + 0*2*2 + 1*2 + 1*1 = 123
```
Szintén az első tagokat egybeolvasva: `1111011` binárisan.

Mivel egy szám a helyiértékek és hatványok szorzatának összege, és bármilyen hatványokat használhatunk, ebből következik, hogy
bármelyik szám bármelyik számrendszerben felírható. Ettől még nincs többféle szám, csak másképp ábrázoljuk ugyanazt az értéket.

Namost a számítógépek mindössze kétféle állapotot értenek meg: van áram, vagy nincs áram. Ezért számukra a kézenfekvő a kettes
számrendszer. Ugyanakkor számunkra a kettes számrendszer macerás, nem túl praktikus, mert sok 0-át vagy 1-est kell leírni hozzá.
De tízes számrendszerbe átváltani sem leányálom (mivel a 10 nem írható le 2-esek összeszorzásával, hiszen 10 = 2\*5). Ezért
találták ki, hogy a 16-os számrendszert használjanak a számítógépeknél. 4 bit az pont 16 (2\*2\*2\*2), ezért 4-es csoportokban
könnyedén átváltható egy kettes számrendszerbeli szám 16-osbelivé. Például 01100010b, az tízesben 98d, de ez nem látszik rajta.
Tizenhatosban pedig 62h, ami könnyebb, mivel az első 4 bit, 0110b az 6, a második fele, 0010b az pedig 2. Összetesszük a kettőt,
és meg is van a 62h. Nem kell az egész számmal számolni, bőven elég mindig csak 4 bitesével.
